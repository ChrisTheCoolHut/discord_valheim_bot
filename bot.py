#!/usr/bin/python3
import os
import subprocess
import syllables
import cmudict
import pickle
import re

haiku_file = "/home/vhserver/haiku_score"
haiku_msg_file = "/home/vhserver/dr_butts_haikus"

import string
syl_dict = cmudict.dict()

def num_syl_cmudict(word):
    '''Count the number of syllables in a word using cmudict
    
    Args:
        - word (str): the word to be counted
        
    Returns:
        - An integer representing the number of syllables, 
          or None if the syllables couldn't be counted
    '''
    # Remove all punctuation from the word
    word = word.translate(str.maketrans('', '', string.punctuation))
    
    # Check if word is in the cmudict
    syls = syl_dict[word]
    
    if len(syls) == 0:
        est = syllables.estimate(word)
        return est
    else:
        ct = 0
        # The cmudict actually lists all sounds in the word. Counting in
        # this way allows you to identify the number of syllables.
        # From StackOverflow:
        # https://datascience.stackexchange.com/questions/23376/how-to-get-the-number-of-syllables-in-a-word
        for sound in syls[0]:
            if sound[-1].isdigit():
                ct += 1
        
        return ct

def is_haiku(sent):
    '''Checks if a given piece of text can be split into the haiku 5-7-5 syllable structure
    
    Args:
        - sent (str): the sentence to be tested
    
    Returns:
        - A boolean indicating whether the text is composed of 
          words that can be separated into a haiku
    '''
    words = sent.split()
    
    count = 0
    hit_5 = False
    hit_7 = False

    # Loop through all the words and check for the 
    # intermediate milestones of a haiku structure
    for word in words:
        num_syl = num_syl_cmudict(word)

        if num_syl is None:
            return False
        
        count += num_syl
        
        # We hit the first five syllables - reset the counter
        if count == 5 and not hit_5:
            count = 0
            hit_5 = True
        
        # If we hit five and then found 7 syllables, 
        # we have our second haiku line
        if count == 7 and hit_5:
            count = 0
            hit_7 = True
    
    #print("{} {} {}".format(hit_5, hit_7, count == 5))
    # If we hit 5 and 7 and there are only 5
    # syllables left, we have a haiku-able text
    return ((count == 5) and hit_5 and hit_7)

import discord
from dotenv import load_dotenv

load_dotenv()
TOKEN = os.getenv("DISCORD_TOKEN")

client = discord.Client()

commands = {}

async def restart_server(message):
    os.system("~/vhserver restart")
    await message.channel.send("Server restarting")

async def restart_cloud_server(message):
    os.system("reboot")
    await message.channel.send("Cloud server restarting")

async def update_server(message):
    os.system("~/vhserver update")

async def print_logs(message):
    pass

async def help_server(message):
    ip_addr = subprocess.check_output(["/usr/bin/curl", "https://ipinfo.io/ip"]).decode()
    response = "```=== Server IP : {} ===\n".format(ip_addr)
    for k,v in commands.items():
        response += "!{} : {}\n".format(k,v[1])

    response += "```"
    await message.channel.send(response)


@client.event
async def on_ready():
    print(f"{client.user} has connected to Discord!")

def log_user(message):

    data = {}
    if os.path.exists(haiku_file):
        with open(haiku_file, 'rb') as f:
            data = pickle.load(f)

    if data is None:
        data = {}

    if message.author.name in data.keys():
        data[message.author.name] += 1
    else:
        data[message.author.name] = 1

    with open(haiku_file, 'wb') as f:
        pickle.dump(data,f)


def log_haiku(message):

    data = []
    if os.path.exists(haiku_msg_file):
        with open(haiku_msg_file, 'rb') as f:
            data = pickle.load(f)

    if data is None:
        data = []

    msg = "{} : {}".format(message.author.name, message.content)
    data.append(msg)

    with open(haiku_msg_file, 'wb') as f:
        pickle.dump(data,f)

async def get_stats(message):
    with open(haiku_file, 'rb') as f:
        data = pickle.load(f)

    response = ""
    for k,v in data.items():
        response += "{} : {}".format(k,v)

    await message.channel.send(response)

async def get_stats(message):
    with open(haiku_file, 'rb') as f:
        data = pickle.load(f)

    response = ""
    for k,v in data.items():
        response += "{} : {}\n".format(k,v)

    await message.channel.send("```\n{}\n```".format(response))

async def handle_haiku(message):
    if is_haiku(message.content):
        log_user(message)
        log_haiku(message)

async def get_details(message):

    start = b"Valheim Server Details"
    stop = b"vhserver Script Details"
    output = subprocess.check_output(["/home/vhserver/vhserver", "details"])

    response = b""

    at_start = False
    for line in output.split(b'\n'):
        
        if start in line:
            at_start = True
            continue
        if b'========' in line:
            continue
        if stop in line:
            break
        if at_start:
            response += line + b'\n'


    # Get rid of terminal coloring
    response = re.sub("\x1b\\[[0-9][0-9]?m","",response.decode())

    await message.channel.send("```\n{}\n```".format(response))

async def get_mem(message):

    output = subprocess.check_output(["free", "-mh"])

    await message.channel.send("```\n{}\n```".format(output.decode()))

commands = {
    "livheim": (restart_server, "Restarts the Valheim server"),
    "laughheim": (restart_cloud_server, "Restarts the cloud server"),
    "update": (update_server, "Updates the Valheim server"),
    "details": (get_details, "Prints server details"),
    "memory": (get_mem, "Prints memory usage"),
    "secret_stats": (get_stats, "Prints stats for the secret game"),
    "help": (help_server, "Prints this help text"),
}

@client.event
async def on_message(message):
    if message.author == client.user:
        return

    await handle_haiku(message)

    if "valheim" not in message.channel.name.lower():
        return

    user_roles = message.author.roles
    # Must be in a nerd group
    nerd_groups = [x for x in user_roles if "nerd" in x.name.lower()]
    if len(nerd_groups) == 0:
        print("No command allowed for {}".format(message.author))
        await message.channel.send(
            "{} you have to be a nerd to interact with the server".format(
                message.author
            )
        )
        return

    if message.content.startswith("!"):
        command = message.content.lower()
        command = command.replace("!","")
        if command in commands.keys():
            func = commands[command.lower()][0]
            await func(message)
        else:
            await message.channel.send("Invalid command {}".format(message.content))


client.run(TOKEN)
